# LinguisticToolBox

The idea is to put together small scripts / functions / tools in different programming languages for small tasks concerning linguistic problems.

## Architecture

### Algorithm
Takes the text (and maybe more parameters) and returns anything calculated. E. g. tokens, dictionaries, frequencies..

### Processor
Basically takes a text (maybe more parameters) and returns a text. 
Processes the text in some way.

## Javascript

### ngram

#### ngram (text, n)
Returns a dictionary with ngrams of length *n* of *text* as keys, their frequency as value.

### DoubleWhitespaceRemover

#### removeDoubleWhitespaces(text)
Removes all double whitespaces and replaces them with a single space

### WhitespaceRemover

#### removeWhitespaces(text)
Removes all whitespaces
