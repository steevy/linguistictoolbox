function removeDoubleWhitespaces(text) {
    return text.replace(/\s+/g, " ");
}
