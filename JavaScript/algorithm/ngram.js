function ngram(text, n) {
    var ngrams = {};

    for (var i = 0; i < text.length - n; i++) {
        var currentNgram = text.substr(i, n);
        ngrams[currentNgram] = currentNgram in ngrams ? ngrams[currentNgram] + 1 : 1;
    }

    return ngrams;
}
